# Tugolut?! #

A thread-safe, in-memory, 'Revolutionary', RESTful API suite to manage accounts and transfer funds between accounts.

## Getting Started
Build the program via maven with the following command:

```bash
$ mvn clean install
```

This command;

* Builds the package, 
* Runs unit + integration tests, 
* Creates open-api client,
* Generates the code coverage report with JaCoCo
* Exports a fat executable jar

Then, if you want to start to service, simply navigate through;

```bash
$ cd target/
$ java -jar tugolut-jar-with-dependencies.jar
```

This will bootstrap the application in port `1881` (sorry, no flexible port configuration, but feel free to change it in ApplicationModule. Promise, only one place to change). The API has integration with Open API - Swagger, which runs on root (`/`). So simply navigate to [http://localhost:1881/](http://localhost:1881/) to access to Swagger UI. You can also check [swagger-docs](http://localhost:1881/swagger-docs) for the OpenAPI doc. If you don't like Swagger, you can also use [pull](https://www.getpostman.com/collections/f744578978e92fef4dcb) the postman collection and play with the service.

Interested in consuming the service from another service (why not?) You can find the relevant classes for the java client (only java supported for now) for the API under `target/src/generated-sources/openapi/src/gen/java/main`.

Interested in code coverage? Have a look at the JaCoCo result under `target/site/jacoco/index.html`

## What are the technologies used?

**Source:**

* Java 8
* Javalin 3.7.0 - as a fast RESTful API framework. 
* Guice 4.2.2
* Lombok 1.18.12 - because noone likes Vanilla anymore :(

**Testing:**

* Junit 4.13
* Mockito 3.3.0
* Hamcrest 2.2
* Unirest 3.6.01 - for integration tests

## API Documentation
Please refer to either `api.json` under `src/main/resources` or execute the jar and navigate to [Swagger-UI](http://localhost:1881/) for full API documentation. Also, the code is self-explanatory and even the smallest components contain javadocs.

## Contact
Any questions? Drop me an [email](mailto:tugcem.oral@gmail.com).