package com.revolut.tugolut.controller.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.javalin.core.validation.Validator;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PathParameterReadersTest {
	
	@Mock
	Context context;
	
	@Test
	public void testValidIntegerPathParam() {
		// given
		String key = "valid_key";
		String validValue = "1";
		when(context.pathParam(key, Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, validValue));

		// when
		int value = PathParameterReaders.validateIntegerPathParam(context, key);
		
		// then
		assertThat(value, is(1));
	}
	
	@Test
	public void testInvalidIntegerPathParam() {
		// given
		String key = "valid_key";
		String invalidValue = "-1";
		when(context.pathParam(key, Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, invalidValue));

		// when
		ThrowingRunnable runnable = () -> PathParameterReaders.validateIntegerPathParam(context, key);

		// then expect
		assertThrows(BadRequestResponse.class, runnable);
	}
}
