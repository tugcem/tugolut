package com.revolut.tugolut.controller.utils;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import com.revolut.tugolut.exception.InvalidAmountException;

public class ValidatorsTest {

	@Test
	public void testValidAmount() {
		try {
			// given / when
			Validators.validateAmount(1d);
		} catch (InvalidAmountException e) {
			// then
			fail("This line should never be executed.");
		}
	}

	@Test
	public void testInvalidAmount() {
		try {
			// given / when
			Validators.validateAmount(-1d);
			fail("This line should never be executed.");
		} catch (InvalidAmountException e) {
			// then
			assertThat(e.getMessage(), is("Requested amount is invalid: -1"));
		}
	}

}
