package com.revolut.tugolut.controller;

import static com.revolut.tugolut.utils.TugolutTestUtils.createAccount;
import static com.revolut.tugolut.utils.TugolutTestUtils.createAccounts;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InternalServiceException;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.io.CreateAccountRequest;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;
import com.revolut.tugolut.repository.AccountRepository;

import io.javalin.core.validation.Validator;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.http.NotFoundResponse;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountControllerTest {

	@Mock
	Context context;

	@Mock
	AccountRepository repository;

	@InjectMocks
	AccountController controller;

	@Test
	public void testGetAll() {
		// given
		List<Account> accounts = createAccounts();
		when(repository.getAll()).thenReturn(accounts);

		// when
		controller.getAll(context);

		// then
		verify(context).json(accounts);
		verify(context).status(200);
		verify(repository).getAll();
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void testGetByExistingID() throws AccountNotFoundException {
		// given
		Account account = createAccount(1, 10, Currency.GBP);
		when(repository.getByID(1)).thenReturn(account);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));

		// when
		controller.getByID(context);

		// then
		verify(context).json(account);
		verify(context).status(200);
		verify(repository).getByID(1);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void testGetByNonExistingID() throws AccountNotFoundException {
		// given
		AccountNotFoundException exc = new AccountNotFoundException(1);
		when(repository.getByID(1)).thenThrow(exc);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));

		// when
		ThrowingRunnable runnable = () -> controller.getByID(context);

		// then
		assertThrows(NotFoundResponse.class, runnable);
		verify(repository).getByID(1);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void testCreateAccount() throws InternalServiceException {
		// given: setup a happy path.
		CreateAccountRequest request = new CreateAccountRequest(10d, Currency.GBP);
		when(context.bodyAsClass(CreateAccountRequest.class)).thenReturn(request);
		Account account = createAccount(1, 10d, Currency.GBP);
		when(repository.create(10d, Currency.GBP)).thenReturn(account);

		// when
		controller.create(context);

		// then
		verify(context).json(account);
		verify(context).status(201);
		verify(repository).create(10d, Currency.GBP);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void testCreateAccountWithInvalidBalance() throws InternalServiceException {
		// given: create request with invalid balance.
		CreateAccountRequest request = new CreateAccountRequest(-10d, Currency.GBP);
		when(context.bodyAsClass(CreateAccountRequest.class)).thenReturn(request);

		// when
		ThrowingRunnable runnable = () -> controller.create(context);

		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(repository);
	}

	@Test
	public void testCreateAccountWithInternalServiceException() throws InternalServiceException {
		// given: setup repository to throw an internal exception.
		CreateAccountRequest request = new CreateAccountRequest(10d, Currency.GBP);
		when(context.bodyAsClass(CreateAccountRequest.class)).thenReturn(request);
		InternalServiceException exception = new InternalServiceException(new IllegalStateException());
		when(repository.create(10d, Currency.GBP)).thenThrow(exception);

		// when
		ThrowingRunnable runnable = () -> controller.create(context);

		// then
		assertThrows(InternalServerErrorResponse.class, runnable);
		verify(repository).create(10d, Currency.GBP);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testWithdraw() throws AccountNotFoundException, InsufficientBalanceException { 
		// given: setup the context and repo for happy path.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		Account account = createAccount(1, 200, Currency.GBP);
		when(repository.withdraw(1, 100)).thenReturn(account);
		
		// when
		controller.withdraw(context);
		
		// then
		verify(context).json(account);
		verify(context).status(200);
		verify(repository).withdraw(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testWithdrawFromNonExistingAccount() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup a non-existing account case.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		AccountNotFoundException anfe = new AccountNotFoundException(1);
		when(repository.withdraw(1, 100)).thenThrow(anfe);
		
		// when
		ThrowingRunnable runnable = () -> controller.withdraw(context);
		
		// then
		assertThrows(NotFoundResponse.class, runnable);
		verify(repository).withdraw(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testWithdrawInsufficientBalance() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup a case where the client requests to withdraw too much funds.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));

		Account account = createAccount(1, 50, Currency.GBP);
		InsufficientBalanceException ibe = new InsufficientBalanceException(account);
		when(repository.withdraw(1, 100)).thenThrow(ibe);
		
		// when
		ThrowingRunnable runnable = () -> controller.withdraw(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verify(repository).withdraw(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testWithdrawInvalidAmount() {
		// given: setup a case where the client requests to withdraw invalid balance.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(-100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		// when
		ThrowingRunnable runnable = () -> controller.withdraw(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(repository);
	}
	
	@Test
	public void testCredit() throws AccountNotFoundException {
		// given: setup happy path
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		Account account = createAccount(1, 200, Currency.GBP);
		when(repository.credit(1, 100)).thenReturn(account);
		
		// when
		controller.credit(context);
		
		// then
		verify(context).json(account);
		verify(context).status(200);
		verify(repository).credit(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testCreditToNonExistingAccount() throws AccountNotFoundException {
		// given: setup a non-existing account case.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		AccountNotFoundException anfe = new AccountNotFoundException(1);
		when(repository.credit(1, 100)).thenThrow(anfe);
		
		// when
		ThrowingRunnable runnable = () -> controller.credit(context);
		
		// then
		assertThrows(NotFoundResponse.class, runnable);
		verify(repository).credit(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testCreditInvalidAmount() {
		// given: setup a case where the client requests to credit invalid balance.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(-100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		// when
		ThrowingRunnable runnable = () -> controller.credit(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(repository);
	}
	
	@Test
	public void testDeleteAccount() throws AccountNotFoundException {
		// given: setup happy path
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		Account account = createAccount(1, 200, Currency.GBP);
		when(repository.delete(1)).thenReturn(account);
		
		// when
		controller.deleteByID(context);
		
		// then
		verify(context).json(account);
		verify(context).status(200);
		verify(repository).delete(1);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testDeleteNonExistingAccount() throws AccountNotFoundException {
		// given: setup a non-existing account case.
		when(context.pathParam("accountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		
		AccountNotFoundException anfe = new AccountNotFoundException(1);
		when(repository.delete(1)).thenThrow(anfe);
		
		// when
		ThrowingRunnable runnable = () -> controller.deleteByID(context);
		
		// then
		assertThrows(NotFoundResponse.class, runnable);
		verify(repository).delete(1);
		verifyNoMoreInteractions(repository);
	}
}
