package com.revolut.tugolut.controller;

import static com.revolut.tugolut.utils.TugolutTestUtils.createAccount;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.external.CurrencyExchangeService;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;
import com.revolut.tugolut.repository.AccountRepository;

import io.javalin.core.validation.Validator;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TransferControllerTest {

	@Mock
	Context context;
	
	@Mock
	AccountRepository repository;
	
	@Mock
	CurrencyExchangeService exchangeService;
	
	@InjectMocks
	TransferController controller;
	
	@Test
	public void testTransferHappyPathSameCurrency() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the happy path
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		Account fromAccount = createAccount(1, 100, Currency.GBP);
		Account toAccount = createAccount(2, 10, Currency.GBP);
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "2"));

		when(repository.getByID(1)).thenReturn(fromAccount);
		when(repository.getByID(2)).thenReturn(toAccount);
		
		when(exchangeService.convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount())).thenReturn(100d);
		
		// when
		controller.transfer(context);
		
		// then
		verify(exchangeService, times(1)).convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount());
		verify(repository).getByID(1);
		verify(repository).getByID(2);
		verify(repository).withdraw(1, 100);
		verify(repository).credit(2, 100);
		verify(context).status(204);
	}
	
	@Test
	public void testTransferHappyPathDifferentCurrency() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the happy path
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		Account fromAccount = createAccount(1, 100, Currency.GBP);
		Account toAccount = createAccount(2, 10, Currency.EUR);
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "2"));

		when(repository.getByID(1)).thenReturn(fromAccount);
		when(repository.getByID(2)).thenReturn(toAccount);
		
		when(exchangeService.convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount())).thenReturn(120d);
		
		// when
		controller.transfer(context);
		
		// then
		verify(exchangeService, times(1)).convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount());
		verify(repository).getByID(1);
		verify(repository).getByID(2);
		verify(repository).withdraw(1, 100);
		verify(repository).credit(2, 120);
		verify(context).status(204);
	}
	
	@Test
	public void testTransferWithInvalidAccountId() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup an invalid account id case.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "-1"));
		
		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(exchangeService);
		verifyNoInteractions(repository);
	}
	
	@Test
	public void testTransferSelfTransfer() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the case where from / to account ids are same.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));

		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(exchangeService);
		verifyNoInteractions(repository);
	}
	
	@Test
	public void testTransferFromAccountNotFound() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the case where from account is not found.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "2"));

		AccountNotFoundException anfe = new AccountNotFoundException(1);
		when(repository.getByID(1)).thenThrow(anfe);
		
		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(NotFoundResponse.class, runnable);
		verifyNoInteractions(exchangeService);
		verify(repository).getByID(1);
		verifyNoMoreInteractions(repository);

	}
	
	@Test
	public void testTransferToAccountNotFound() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the case where to account is not found.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		Account fromAccount = createAccount(1, 100, Currency.GBP);
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "2"));

		AccountNotFoundException anfe = new AccountNotFoundException(1);
		when(repository.getByID(1)).thenReturn(fromAccount);
		when(repository.getByID(2)).thenThrow(anfe);
		
		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(NotFoundResponse.class, runnable);
		verifyNoInteractions(exchangeService);
		verify(repository).getByID(1);
		verify(repository).getByID(2);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testTransferInvalidAmount() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the case where invalid amount is requested to transfer.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(-100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verifyNoInteractions(exchangeService);
		verifyNoMoreInteractions(repository);
	}
	
	@Test
	public void testTransferInsufficientBalance() throws AccountNotFoundException, InsufficientBalanceException {
		// given: setup the case where the funds in 'from' account is insufficient.
		UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(100).build();
		when(context.bodyAsClass(UpdateBalanceRequest.class)).thenReturn(request);
		
		Account fromAccount = createAccount(1, 50, Currency.GBP);
		Account toAccount = createAccount(2, 10, Currency.GBP);
		when(context.pathParam("fromAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "1"));
		when(context.pathParam("toAccountId", Integer.class)).thenReturn(Validator.<Integer>create(Integer.class, "2"));

		when(repository.getByID(1)).thenReturn(fromAccount);
		when(repository.getByID(2)).thenReturn(toAccount);
		
		InsufficientBalanceException ibe = new InsufficientBalanceException(fromAccount);
		when(repository.withdraw(1, 100)).thenThrow(ibe);
		
		when(exchangeService.convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount())).thenReturn(50d);
		
		// when
		ThrowingRunnable runnable = () -> controller.transfer(context);
		
		// then
		assertThrows(BadRequestResponse.class, runnable);
		verify(exchangeService, times(1)).convert(fromAccount.getCurrency(), toAccount.getCurrency(), request.getAmount());
		verify(repository).getByID(1);
		verify(repository).getByID(2);
		verify(repository).withdraw(1, 100);
		verifyNoMoreInteractions(repository);
	}
	
}
