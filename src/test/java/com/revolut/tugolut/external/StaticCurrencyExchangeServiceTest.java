package com.revolut.tugolut.external;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.module.ExternalDependenciesModule;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class StaticCurrencyExchangeServiceTest {

	@Before
	public void setup() {
		// create the injector with only dependencies module.
		Guice.createInjector(new ExternalDependenciesModule()).injectMembers(this);
	}

	@Inject
	CurrencyExchangeService exchangeService;

	@Test
	public void testCorrectInjection() {
		assertThat(exchangeService, is(notNullValue()));
		assertThat(exchangeService, is(instanceOf(StaticCurrencyExchangeService.class)));
	}
	
	@Test
	public void testConvertSameCurrency() {
		// given / when
		double converted = exchangeService.convert(Currency.EUR, Currency.EUR, 100);

		// then
		assertThat(converted, is(100.0d));
	}
	
	@Test
	public void testConvertDifferentCurrency() {
		// given / when
		double converted = exchangeService.convert(Currency.GBP, Currency.EUR, 100);

		// then
		assertThat(converted, is(115.60100));
	}

}
