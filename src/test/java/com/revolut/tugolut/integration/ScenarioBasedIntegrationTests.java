package com.revolut.tugolut.integration;

import static com.revolut.tugolut.utils.TugolutTestUtils.accountsUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.Test;

import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;

import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

/**
 * This class have some complex integration tests not in 'given - when - then' fashion.
 */
public class ScenarioBasedIntegrationTests extends BaseIntegrationTest {
    
    @Test
    public void testTransferBetweenAccounts() {
        // create accounts
        Account account1 = createAccount(100, Currency.EUR);
        Account account2 = createAccount(50, Currency.GBP);
        Account account3 = createAccount(1000, Currency.TRY);
        
        // confirm that these accounts are persisted.
        HttpResponse<List<Account>> getAllResponse = Unirest.get(accountsUrl(port))
                .asObject(new GenericType<List<Account>>() {
                });

        assertThat(getAllResponse.getStatus(), is(200));
        assertThat(getAllResponse.getBody().size(), is(3));
        assertThat(getAllResponse.getBody(), hasItems(account1, account2, account3));
        
        // make some transfers between accounts.
        // 1 - 50 EUR -> 2 // 1: 50, 2: 50 + 43.252 = 93.252
        HttpResponse<?> transfer1Reponse = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(UpdateBalanceRequest.builder().amount(50).build())
                .asEmpty();
        
        // 1 - 50 EUR -> 3 // 1: 0, 3: 1000 + 343.849 = 1343.849
        HttpResponse<?> transfer2Reponse = Unirest.post(accountsUrl(port, "/1", "/transfer", "/3"))
                .body(UpdateBalanceRequest.builder().amount(50).build())
                .asEmpty();

        // 1 - 10 EUR -> 2 // insufficient balance
        HttpResponse<?> transfer3Reponse = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(UpdateBalanceRequest.builder().amount(50).build())
                .asEmpty();

        assertThat(transfer1Reponse.getStatus(), is(204));
        assertThat(transfer2Reponse.getStatus(), is(204));
        assertThat(transfer3Reponse.getStatus(), is(400));
        
        // get latest state of accounts:
        Account updatedAccount1 = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        Account updatedAccount2 = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();
        Account updatedAccount3 = Unirest.get(accountsUrl(port, "/3")).asObject(Account.class).getBody();
        
        assertThat(updatedAccount1.getBalance(), is(0d));
        assertThat(updatedAccount2.getBalance(), is(93.25200000000001d));
        assertThat(updatedAccount3.getBalance(), is(1343.849));
        
    }
}
