package com.revolut.tugolut.integration;

import static com.revolut.tugolut.utils.TugolutTestUtils.accountsUrl;
import static com.revolut.tugolut.utils.TugolutTestUtils.createBadRequestError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import com.revolut.tugolut.model.ErrorResponse;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

/**
 * This integration test class uses raw json requests rather than type-safe
 * client. This is to ensure serialization / deserialization is handled in API
 * level.
 */
public class RawIntegrationTests extends BaseIntegrationTest {

    @Test
    public void testCreateAccountWithInvalidCurrency() {
        // @formatter:off
        // given: create expected error and an invalid request to pass.
        String rawCreateRequest = "{ \"balance\": 100, \"currency\": \"CAD\" }";
        ErrorResponse expectedError = createBadRequestError("Couldn't deserialize body to CreateAccountRequest");
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .post(accountsUrl(port))
                .body(rawCreateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have the item being created.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));        
    }
    
    @Test
    public void testWithdrawWithInvalidInput() {
        // @formatter:off
        // given: create expected error and an invalid request to pass.
        String rawUpdateRequest = "{ \"unknown_field\": 100 }";
        ErrorResponse expectedError = createBadRequestError("Couldn't deserialize body to UpdateBalanceRequest");
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/withdraw"))
                .body(rawUpdateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have the item being created.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));        
    }
}
