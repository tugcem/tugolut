package com.revolut.tugolut.integration;

import static com.revolut.tugolut.utils.TugolutTestUtils.accountsUrl;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.io.CreateAccountRequest;
import com.revolut.tugolut.module.ApplicationModule;

import io.javalin.Javalin;
import kong.unirest.Unirest;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * Base integration test class used by all integration tests.
 */
@Ignore
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class BaseIntegrationTest {

    @Inject
    Javalin javalin;

    @Inject
    @Named("Http.Port")
    int port;
    
    @Before
    public void setup() throws Exception {
        // create the injector with application module.
        Guice.createInjector(new ApplicationModule()).injectMembers(this);

        // start the container.
        javalin.start(port);

        // make sure that the server is up - running all the tests consequently causes
        // some of them to fail due to NoHttpResponseException / SocketException as the
        // container is finalized but sockets are not released yet.
        Thread.sleep(1000);
    }
    
    @After
    public void tearDown() throws Exception {
        // wrap up the container.
        javalin.stop();
        
        // make sure that the server is down - running all the tests consequently causes
        // some of them to fail due to NoHttpResponseException / SocketException as the 
        // container is finalized but sockets are not released yet.
        Thread.sleep(1000);
    }
    
    /**
     * Internal helper method to send a post request to create a default account.
     * @return created account
     */
    protected Account createAccount() {
        return this.createAccount(100d, Currency.GBP);
    }
    
    /**
     * Internal helper method to send a post request to create a custom account.
     * @param amount the opening balance
     * @param currency the currency of the account
     * @return created account
     */
    protected Account createAccount(double amount, Currency currency) {
        return Unirest.post(accountsUrl(port)).body(new CreateAccountRequest(amount, currency)).asObject(Account.class).getBody();
    }
}
