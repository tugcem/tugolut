package com.revolut.tugolut.integration;

import static com.revolut.tugolut.utils.TugolutTestUtils.accountsUrl;
import static com.revolut.tugolut.utils.TugolutTestUtils.createBadRequestError;
import static com.revolut.tugolut.utils.TugolutTestUtils.createNotFoundError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.Test;

import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.ErrorResponse;
import com.revolut.tugolut.model.io.CreateAccountRequest;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;

import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * This class is aimed to run isolated integration tests where each test works
 * in an isolated fashion, each starts up a Javalin instance before beginning
 * and closing it after a successful execution. Since we're using an in-memory
 * storage layer, terminating the container would kill the process and the
 * storage is wiped out. This is handy to test edge cases and basic operations.
 * For more complex -scenario based- integration tests, please refer to
 * {@link ScenarioBasedIntegrationTests} class.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TypeSafeIntegrationTests extends BaseIntegrationTest {

    @Test
    public void testGetAllEmptyResponse() {
        // given / when
        HttpResponse<List<Account>> response = Unirest.get(accountsUrl(port))
                .asObject(new GenericType<List<Account>>() {
                });

        // then: expect a 200 response with empty list.
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody(), is(empty()));
    }

    @Test
    public void testGetAllFullResponse() {
        // given
        Account expectedAccount1 = createAccount();
        Account expectedAccount2 = createAccount(200d, Currency.GBP);

        // when
        HttpResponse<List<Account>> response = Unirest.get(accountsUrl(port))
                .asObject(new GenericType<List<Account>>() {
                });

        // then: expect a 200 response with empty list.
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody().size(), is(2));
        assertThat(response.getBody(), hasItems(expectedAccount1, expectedAccount2));
    }

    @Test
    public void testGetAccountHappyPath() {
        // given
        Account expectedAccount = createAccount();

        // when
        HttpResponse<Account> response = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class);

        // then: expect that the response have the item being created.
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody(), is(equalTo(expectedAccount)));
    }

    @Test
    public void testGetNonExistingAccount() {
        // given: create expected error response.
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 1");

        // when
        HttpResponse<ErrorResponse> response = Unirest.get(accountsUrl(port, "/1")).asObject(ErrorResponse.class);

        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(equalTo(expectedError)));
    }

    @Test
    public void testCreateAccountHappyPath() {
        // @formatter:off
		// given: create expected account and the request to pass.
		Account expectedAccount = Account.builder()
				.id(1)
				.balance(100d)
				.currency(Currency.GBP)
			.build();
		CreateAccountRequest createRequest = new CreateAccountRequest(100d, Currency.GBP);
		
		// when 
		HttpResponse<Account> response = Unirest
				.post(accountsUrl(port))
				.body(createRequest)
			.asObject(Account.class);
		// @formetter:on
		
		// then: expect that the response have the item being created.
		assertThat(response.getStatus(), is(201));
		assertThat(response.getBody(), is(equalTo(expectedAccount)));
	}

    @Test
    public void testCreateWithInvalidBalance() {
        // @formatter:off
        // given: create expected error response and invalid request to pass.
        ErrorResponse expectedError = createBadRequestError("Requested amount is invalid: -100");
        CreateAccountRequest createRequest = new CreateAccountRequest(-100d, Currency.GBP);
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .post(accountsUrl(port))
                .body(createRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));
    }
    
    @Test
    public void testWithdrawHappyPath() {
        // given: create account and 'expected' updated account.
        createAccount();
        Account expectedAccount = Account.builder()
                .id(1)
                .balance(0d)
                .currency(Currency.GBP)
            .build();
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(100).build();
        
        // when 
        HttpResponse<Account> response = Unirest
                .patch(accountsUrl(port, "/1", "/withdraw"))
                .body(updateRequest)
            .asObject(Account.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody(), is(equalTo(expectedAccount)));
        
        // also get account should retrieve the same account information.
        Account getAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        assertThat(getAccountResponse, is(equalTo(expectedAccount)));
    }
    
    @Test
    public void testWithdrawFromNonExistingAccount() {
        // @formatter:off
        // given: create expected error and the request to pass.
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 1");
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(100d).build();
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/withdraw"))
                .body(updateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(equalTo(expectedError)));
    }
    
    @Test
    public void testWithdrawInvalidAmount() {
        // given
        Account expectedAccount = createAccount();
        ErrorResponse expectedError = createBadRequestError("Requested amount is invalid: -100");
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(-100d).build();
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/withdraw"))
                .body(updateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));
        
        // also get account should retrieve the unchanged existing account information.
        Account getAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        assertThat(getAccountResponse, is(equalTo(expectedAccount)));
    }

    @Test
    public void testWithdrawInsufficientBalance() {
        // given
        Account expectedAccount = createAccount();
        ErrorResponse expectedError = createBadRequestError(
                "Account: Account(id=1, balance=100.0, currency=GBP) does not have enough balance.");
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(1000d).build();
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/withdraw"))
                .body(updateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));
        
        // also get account should retrieve the unchanged existing account information.
        Account getAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        assertThat(getAccountResponse, is(equalTo(expectedAccount)));
    }
    
    @Test
    public void testCreditHappyPath() {
        // given: create account and 'expected' new account object.
        createAccount();
        Account expectedAccount = Account.builder()
                .id(1)
                .balance(200d)
                .currency(Currency.GBP)
            .build();
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(100d).build();
        
        // when 
        HttpResponse<Account> response = Unirest
                .patch(accountsUrl(port, "/1", "/credit"))
                .body(updateRequest)
            .asObject(Account.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody(), is(equalTo(expectedAccount)));
        
        // also get account should retrieve the same account information.
        Account getAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        assertThat(getAccountResponse, is(equalTo(expectedAccount)));
    }
    
    @Test
    public void testCreditToNonExistingAccount() {
        // @formatter:off
        // given: create expected error and the request to pass.
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 1");
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(100d).build();
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/credit"))
                .body(updateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(equalTo(expectedError)));
    }
    
    @Test
    public void testCreditInvalidAmount() {
        // given
        Account expectedAccount = createAccount();
        ErrorResponse expectedError = createBadRequestError("Requested amount is invalid: -100");
        UpdateBalanceRequest updateRequest = UpdateBalanceRequest.builder().amount(-100d).build();
        
        // when 
        HttpResponse<ErrorResponse> response = Unirest
                .patch(accountsUrl(port, "/1", "/credit"))
                .body(updateRequest)
            .asObject(ErrorResponse.class);
        // @formetter:on
        
        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(equalTo(expectedError)));
        
        // also get account should retrieve the unchanged existing account information.
        Account getAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();
        assertThat(getAccountResponse, is(equalTo(expectedAccount)));
    }
    
    @Test
    public void testDeleteAccount() {
        // given: create the account and expected error.
        Account expectedAccount = createAccount();
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 1");

        // when
        HttpResponse<Account> response = Unirest.delete(accountsUrl(port, "/1")).asObject(Account.class);

        // then
        assertThat(response.getStatus(), is(200));
        assertThat(response.getBody(), is(equalTo(expectedAccount)));
        
        // also get account should not retrieve any account.
        HttpResponse<ErrorResponse> getResponse = Unirest.get(accountsUrl(port, "/1")).asObject(ErrorResponse.class);
        assertThat(getResponse.getStatus(), is(404));
        assertThat(getResponse.getBody(), is(equalTo(expectedError)));
    }

    @Test
    public void testDeleteNonExistingAccount() {
        // given: create expected error and the request to pass.
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 1");        
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.delete(accountsUrl(port, "/1")).asObject(ErrorResponse.class);

        // then: expect that the response have a client error.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(equalTo(expectedError)));
    }
    
    @Test
    public void testTransferHappyPathSameCurrency() {
        // given: create accounts and post them.
        createAccount(100d, Currency.GBP);
        createAccount(200d, Currency.GBP);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        
        // when
        HttpResponse<?> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(request)
                .asEmpty();

        // then
        assertThat(response.getStatus(), is(204));
        
        // retrieve the updated account information.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse.getBalance(), is(50d));
        assertThat(getSecondAccountResponse.getBalance(), is(250d));
    }
    
    @Test
    public void testTransferHappyPathDifferentCurrency() {
        // given: create accounts and post them.
        createAccount(100d, Currency.GBP);
        createAccount(200d, Currency.EUR);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        
        // when
        HttpResponse<?> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(request)
                .asEmpty();

        // then
        assertThat(response.getStatus(), is(204));
        
        // retrieve the updated account information.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse.getBalance(), is(50d));
        // expect 50 GBP translated to 57.8005 EUR
        assertThat(getSecondAccountResponse.getBalance(), is(257.8005));
    }
    
    @Test
    public void testTransferWithEmptyPayload() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(100d, Currency.GBP);
        Account secondAccount = createAccount(200d, Currency.EUR);
        ErrorResponse expectedError = createBadRequestError("Couldn't deserialize body to UpdateBalanceRequest");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2")).asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
        assertThat(getSecondAccountResponse, is(equalTo(secondAccount)));
    }
    
    @Test
    public void testSelfTransfer() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(100d, Currency.GBP);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();

        ErrorResponse expectedError = createBadRequestError("Self-transfer is not allowed. Requested account id: 1");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/1"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
    }
    
    @Test
    public void testTransferInsufficientBalance() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(10d, Currency.GBP);
        Account secondAccount = createAccount(200d, Currency.EUR);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        
        ErrorResponse expectedError = createBadRequestError("Account: Account(id=1, balance=10.0, currency=GBP) does not have enough balance.");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
        assertThat(getSecondAccountResponse, is(equalTo(secondAccount)));
    }
    
    @Test
    public void testTransferInvalidAmount() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(10d, Currency.GBP);
        Account secondAccount = createAccount(200d, Currency.EUR);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(-50).build();
        
        ErrorResponse expectedError = createBadRequestError("Requested amount is invalid: -50");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
        assertThat(getSecondAccountResponse, is(equalTo(secondAccount)));
    }
    
    @Test
    public void testTransferWithInvalidAccountId() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(100d, Currency.GBP);
        Account secondAccount = createAccount(200d, Currency.EUR);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        
        ErrorResponse expectedError = createBadRequestError("Path parameter 'fromAccountId' with value 'from_invalid' is not a valid Integer");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/from_invalid", "/transfer", "/to_invalid"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(400));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        Account getSecondAccountResponse = Unirest.get(accountsUrl(port, "/2")).asObject(Account.class).getBody();        
        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
        assertThat(getSecondAccountResponse, is(equalTo(secondAccount)));
    }
    
    @Test
    public void testTransferToNonExistingAccount() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(100d, Currency.GBP);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 2");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/1", "/transfer", "/2"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
    }
    
    @Test
    public void testTransferFromNonExistingAccount() {
        // given: create accounts and post them.
        Account firstAccount = createAccount(200d, Currency.EUR);
        UpdateBalanceRequest request = UpdateBalanceRequest.builder().amount(50).build();
        
        ErrorResponse expectedError = createNotFoundError("Account not found for given account id: 2");
        
        // when
        HttpResponse<ErrorResponse> response = Unirest.post(accountsUrl(port, "/2", "/transfer", "/1"))
                .body(request)
                .asObject(ErrorResponse.class);

        // then: receive a failed response.
        assertThat(response.getStatus(), is(404));
        assertThat(response.getBody(), is(expectedError));
        
        // retrieve the account information hasn't been changed.
        Account getFirstAccountResponse = Unirest.get(accountsUrl(port, "/1")).asObject(Account.class).getBody();        
        assertThat(getFirstAccountResponse, is(equalTo(firstAccount)));
    }
}
