package com.revolut.tugolut.repository;

import static com.revolut.tugolut.utils.TugolutTestUtils.createAccounts;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InternalServiceException;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class InMemoryAccountRepositoryTest {

	InMemoryAccountRepository repository;

	@Before
	public void setup() {
		// use a clean repository instance every time.
		repository = new InMemoryAccountRepository();
	}

	@Test
	public void testGetAll() {
		// when
		List<Account> accounts = repository.getAll();

		// then
		assertThat(accounts, is(empty()));
	}

	@Test
	public void testGetAllWithMultipleAccounts() throws Exception {
		// given: yes, no batch support.
		List<Account> accounts = createAccounts();
		for (Account account : accounts) {
			repository.create(account.getBalance(), account.getCurrency());
		}

		// when
		List<Account> fetchedAccounts = repository.getAll();

		// then
		assertThat(fetchedAccounts, is(equalTo(accounts)));
	}

	@Test
	public void testGetByIDAccountNotFound() throws AccountNotFoundException {
		// when
		ThrowingRunnable runnable = () -> repository.getByID(1);

		// then - expect
		assertThrows(AccountNotFoundException.class, runnable);
	}

	@Test
	public void testGetByID() throws AccountNotFoundException, InternalServiceException {
		// given
		repository.create(100d, Currency.GBP);

		// when: the first assigned id should be 1.
		Account fetchedAccount = repository.getByID(1);

		// then
		assertThat(fetchedAccount.getId(), is(1));
		assertThat(fetchedAccount.getBalance(), is(100d));
		assertThat(fetchedAccount.getCurrency(), is(Currency.GBP));
	}

	@Test
	public void testCreate() throws InternalServiceException, AccountNotFoundException {
		// given / when: the request validation is done in higher level, not in repository.
		Account account = repository.create(100d, Currency.GBP);

		// then
		assertThat(account.getId(), is(1));
		assertThat(account.getBalance(), is(100d));
		assertThat(account.getCurrency(), is(Currency.GBP));

		// also confirm this has been registered.
		assertThat(repository.getByID(1), is(equalTo(account)));
	}
	
	@Test
	public void testWithdraw() throws AccountNotFoundException, InternalServiceException, InsufficientBalanceException {
		// given: create a new account first.
		repository.create(100d, Currency.GBP);

		// when: withdraw the created amount
		Account updatedAccount = repository.withdraw(1, 100);

		// then: expect a clean balance
		assertThat(updatedAccount.getId(), is(1));
		assertThat(updatedAccount.getBalance(), is(0d));
		assertThat(updatedAccount.getCurrency(), is(Currency.GBP));
		
		// also, the repo have it updated.
		assertThat(repository.getByID(1), is(equalTo(updatedAccount)));
	}
	
	@Test
	public void testWithdrawInsufficientBalance() throws AccountNotFoundException, InternalServiceException, InsufficientBalanceException {
		// given: create a new account first.
		repository.create(10d, Currency.GBP);

		// when: withdraw the too much amount.
		ThrowingRunnable runnable = () -> repository.withdraw(1, 100);

		// then: expect a the exception and data being untouched.
		assertThrows(InsufficientBalanceException.class, runnable);
		
		Account account = repository.getByID(1);
		assertThat(account.getId(), is(1));
		assertThat(account.getBalance(), is(10d));
		assertThat(account.getCurrency(), is(Currency.GBP));		
	}
	
	@Test
	public void testWithdrawAccountNotFound() throws AccountNotFoundException {
		// when
		ThrowingRunnable runnable = () -> repository.withdraw(1, 10);

		// then - expect
		assertThrows(AccountNotFoundException.class, runnable);
	}

	@Test
	public void testCredit() throws InternalServiceException, AccountNotFoundException {
		// given: create a new account first.
		repository.create(100d, Currency.GBP);

		// when: credit to created amount
		Account updatedAccount = repository.credit(1, 100);

		// then: expect a clean balance
		assertThat(updatedAccount.getId(), is(1));
		assertThat(updatedAccount.getBalance(), is(200d));
		assertThat(updatedAccount.getCurrency(), is(Currency.GBP));
		
		// also, the repo have it updated.
		assertThat(repository.getByID(1), is(equalTo(updatedAccount)));
	}
	
	@Test
	public void testCreditAccountNotFound() throws AccountNotFoundException {
		// when
		ThrowingRunnable runnable = () -> repository.credit(1, 10);

		// then - expect
		assertThrows(AccountNotFoundException.class, runnable);
	}
	
	@Test
	public void testDeleteByID() throws InternalServiceException, AccountNotFoundException {
        // given: yes, no batch support.
        List<Account> accounts = createAccounts();
        for (Account account : accounts) {
            repository.create(account.getBalance(), account.getCurrency());
        }

		// when: delete account #2.
		Account deletedAccount = repository.delete(2);

		// then
		assertThat(deletedAccount.getId(), is(2));
		assertThat(deletedAccount.getBalance(), is(10d));
		assertThat(deletedAccount.getCurrency(), is(Currency.EUR));
		
		// also, confirm the rest is untouched:
		List<Account> remainingAccounts = repository.getAll();
		assertThat(remainingAccounts.size(), is(2));
		assertThat(remainingAccounts, contains(accounts.get(0), accounts.get(2)));
	}

	@Test
	public void testDeleteByIDAccountNotFound() throws AccountNotFoundException {
		// when
		ThrowingRunnable runnable = () -> repository.delete(1);

		// then - expect
		assertThrows(AccountNotFoundException.class, runnable);
	}
	
	@Test
	public void testDeleteByIDAfterDeleting() throws InternalServiceException, AccountNotFoundException {
        // given: yes, no batch support.
        List<Account> accounts = createAccounts();
        for (Account account : accounts) {
            repository.create(account.getBalance(), account.getCurrency());
        }

		// delete account #2.
		repository.delete(2);
		
		// when: then, delete same account again.
		ThrowingRunnable runnable = () -> repository.delete(2);

		// then
		assertThrows(AccountNotFoundException.class, runnable);

		// also, confirm the rest is untouched:
		List<Account> remainingAccounts = repository.getAll();
		assertThat(remainingAccounts.size(), is(2));
		assertThat(remainingAccounts, contains(accounts.get(0), accounts.get(2)));
	}

}
