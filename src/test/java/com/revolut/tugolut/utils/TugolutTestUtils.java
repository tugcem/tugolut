package com.revolut.tugolut.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;
import com.revolut.tugolut.model.ErrorResponse;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class TugolutTestUtils {

    static final String DEFAULT_PROTOCOL = "http";

    static final String DEFAULT_HOST = "localhost";

    static final String ACCOUNTS = "/accounts";

    public static List<Account> createAccounts() {
        return ImmutableList.of(createAccount(1, 10, Currency.GBP), createAccount(2, 10, Currency.EUR),
                createAccount(3, 10, Currency.USD));
    }

    public static Account createAccount(int id, double balance, Currency currency) {
        // @formetter:off
        return Account.builder().id(id).balance(balance).currency(currency).build();
        // @formatter:on
    }

    public static String accountsUrl(int port, String... paths) {
        try {
            return new URL(DEFAULT_PROTOCOL, DEFAULT_HOST, port, ACCOUNTS + String.join("", paths)).toString();
        } catch (MalformedURLException mue) {
            // this should never happen, raise it to the test class.
            throw new RuntimeException(mue);
        }
    }

    public static ErrorResponse createBadRequestError(String title) {
        // @formatter:off
        return ErrorResponse.builder()
                .title(title)
                .status(400)
                .type("https://javalin.io/documentation#badrequestresponse")
                .details(ImmutableMap.of())
            .build();
        // @formatter:on
    }

    public static ErrorResponse createNotFoundError(String title) {
        // @formatter:off
        return ErrorResponse.builder()
                .title(title)
                .status(404)
                .type("https://javalin.io/documentation#notfoundresponse")
                .details(ImmutableMap.of())
                .build();
        // @formatter:on
    }

}
