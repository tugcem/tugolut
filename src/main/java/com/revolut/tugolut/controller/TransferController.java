package com.revolut.tugolut.controller;

import static com.revolut.tugolut.controller.utils.PathParameterReaders.validateIntegerPathParam;
import static com.revolut.tugolut.controller.utils.Validators.validateAmount;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InvalidAmountException;
import com.revolut.tugolut.exception.SelfTransferException;
import com.revolut.tugolut.external.CurrencyExchangeService;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.ErrorResponse;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;
import com.revolut.tugolut.repository.AccountRepository;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * A thin layer specifically encapsulating the logic for
 * <code>/accounts/:fromAccountId/transfer/:toAccountId</code> API. Ideally,
 * this class should only define the contract for transfer API, and have basic
 * translation / validation logic. But for the sake of simplification, it has
 * dependencies to some business-related services (like
 * {@link CurrencyExchangeService}) and handles the transfer between given two
 * accounts.
 */
@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TransferController {

	@Inject
	CurrencyExchangeService exchangeService;
	
	@Inject
	AccountRepository repository;
	
    /**
     * Transfers funds between two accounts. Follows the logic: 
     * 
     * <ul>
     * <li>Does business-level input validation.</li>
     * <li>If aforementioned restrictions does not apply, exchanges the requested amount into target account's currency via {@link CurrencyExchangeService}.</li>
     * <li>Withdraws funds from 'from' account.</li>
     * <li>Credits exchanged funds to 'to' account.</li>
     * </ul>
     * 
     * Some restrictions of this API:
     * 
     * <ul>
     * <li>Self-transfer is not allowed, 'from' and 'to' accounts have to be different.</li>
     * <li>Both 'from' and 'to' accounts have to be defined in the system.</li>
     * <li>'from' account has to have sufficient balance.</li>
     * <li>The requested transfer amount has to be positive.</li>
     * </ul>
     * 
     * @param context the context to read path parameters, request body and write the status code and response body.
     */
    @OpenApi(
            summary = "Transfer funds between accounts",
            operationId = "transfer",
            path = "/accounts/:fromAccountId/transfer/:toAccountId",
            method = HttpMethod.POST,
            tags = {"Account"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = UpdateBalanceRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
	public void transfer(Context context) {
    	try {
    		// get + validate from amount
			UpdateBalanceRequest transferRequest = context.bodyAsClass(UpdateBalanceRequest.class);
			double fromAmount = transferRequest.getAmount();
    		validateAmount(fromAmount);
			
			//retrieve from / to accounts
			int fromAccountId = validateIntegerPathParam(context, "fromAccountId");
			int toAccountId = validateIntegerPathParam(context, "toAccountId");
			
			if(fromAccountId == toAccountId) {
				throw new SelfTransferException(fromAccountId);
			}

			// calculate 'to' amount.
			Account fromAccount = repository.getByID(fromAccountId);
			Account toAccount = repository.getByID(toAccountId);
			double toAmount = exchangeService.convert(fromAccount.getCurrency(), toAccount.getCurrency(), fromAmount);
			
			// withdraw from 'from' / credit exchanged amount to 'to'.
			repository.withdraw(fromAccountId, fromAmount);
			repository.credit(toAccountId, toAmount);
			
			// set the status as success - 'No Content' if all went fine.
			context.status(204);
		} catch (AccountNotFoundException anfe) {
			throw new NotFoundResponse(anfe.getMessage());
		} catch (InsufficientBalanceException | SelfTransferException | InvalidAmountException exception) {
			throw new BadRequestResponse(exception.getMessage());
		}
	}
}
