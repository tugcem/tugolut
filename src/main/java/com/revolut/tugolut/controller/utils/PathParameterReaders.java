package com.revolut.tugolut.controller.utils;

import io.javalin.http.Context;

/**
 * A helper class to read path path parameters from Javalin {@link Context}.
 */
public final class PathParameterReaders {

    private PathParameterReaders() {
    }

    // Prevent duplicate validation of account id
    public static int validateIntegerPathParam(Context context, String key) {
        return context.pathParam(key, Integer.class).check(id -> id > 0).get();
    }
}
