package com.revolut.tugolut.controller.utils;

import com.revolut.tugolut.exception.InvalidAmountException;

/**
 * A validator helper class to validate inputs in controller level.
 */
public final class Validators {

    private Validators() {
    }

    public static void validateAmount(Double amount) throws InvalidAmountException {
        if (amount < 0.0) {
            throw new InvalidAmountException(amount);
        }
    }

}
