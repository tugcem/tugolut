package com.revolut.tugolut.controller;

import static com.revolut.tugolut.controller.utils.Validators.*;
import static com.revolut.tugolut.controller.utils.PathParameterReaders.*;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InternalServiceException;
import com.revolut.tugolut.exception.InvalidAmountException;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.ErrorResponse;
import com.revolut.tugolut.model.io.CreateAccountRequest;
import com.revolut.tugolut.model.io.UpdateBalanceRequest;
import com.revolut.tugolut.repository.AccountRepository;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * A thin controller layer that defines the /accounts API contract. Contains the following common pattern for all methods;
 * 
 * <ul>
 * <li>Applies lightweight business-related input validation.</li>
 * <li>Extracts relevant information from path parameters and / or request body.</li>
 * <li>Interacts with {@link AccountRepository} to facilitate the requests, which is the data layer.</li>
 * <li>Handles exceptions thrown from data layer, maps them to proper REST based responses.</li>
 * <li>Updates {@link Context} body and status code with the execution response.</li>
 * </ul>
 */
@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountController {
	
	@Inject
	AccountRepository repository;
	
    /**
     * Finds all accounts. Idempotent by nature.
     */
    @OpenApi(
            summary = "Get all accounts",
            operationId = "getAll",
            path = "/accounts",
            method = HttpMethod.GET,
            tags = {"Account"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Account[].class)})
            }
    )
    public void getAll(Context context) {
        context.json(repository.getAll());
        context.status(200);
    }
    
    /**
     * Finds account by account id, which is extracted from the path parameters of
     * given {@link Context}. Idempotent by nature.
     * 
     * @param context the context
     */
    @OpenApi(
            summary = "Get account by ID",
            operationId = "getByID",
            path = "/accounts/:accountId",
            method = HttpMethod.GET,
            pathParams = {@OpenApiParam(name = "accountId", type = Integer.class, description = "The account ID")},
            tags = {"Account"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Account.class)}),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public void getByID(Context context) {
        try {
			context.json(repository.getByID(validateIntegerPathParam(context, "accountId")));
			context.status(200);
		} catch (AccountNotFoundException anfe) {
			throw new NotFoundResponse(anfe.getMessage());
		}
    }
    
    /**
     * Creates a new account, based on the {@link CreateAccountRequest} extracted
     * from request body, fetch from given {@link Context}. Consequent calls create
     * new accounts.
     * 
     * @param context the context
     */
    @OpenApi(
            summary = "Create account",
            operationId = "create",
            path = "/accounts",
            method = HttpMethod.POST,
            tags = {"Account"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = CreateAccountRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "201"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "500", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public void create(Context context) {
        CreateAccountRequest newAccountRequest = context.bodyAsClass(CreateAccountRequest.class);
        try {
    		// validate requested update amount.
    		validateAmount(newAccountRequest.getBalance());
        	
			context.json(repository.create(newAccountRequest.getBalance(), newAccountRequest.getCurrency()));
			context.status(201);
		} catch (InvalidAmountException iae) {
			throw new BadRequestResponse(iae.getMessage());
		} catch (InternalServiceException ise) {
			throw new InternalServerErrorResponse(ise.getMessage());
		} 
    }
    
    /**
     * Withdraws money from an account. Account id should be in path parameters, the
     * withdrawal amount is passed in the request body via
     * {@link UpdateBalanceRequest}. Used Http.Verb <code>PATCH</code> as it
     * <b>partially</b> updates the account information.
     * 
     * @param context the context.
     */
    @OpenApi(
            summary = "Withdraw money from given account ID",
            operationId = "withdraw",
            path = "/accounts/:accountId/withdraw",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "accountId", type = Integer.class, description = "The account ID")},
            tags = {"Account"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = UpdateBalanceRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Account.class)}),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public void withdraw(Context context) {
    	try {
    		int accountId = validateIntegerPathParam(context, "accountId");
    		UpdateBalanceRequest updateRequest = context.bodyAsClass(UpdateBalanceRequest.class);
    		// validate requested update amount.
    		validateAmount(updateRequest.getAmount());

    		context.json(repository.withdraw(accountId, updateRequest.getAmount()));
    		context.status(200);
		} catch (AccountNotFoundException anfe) {
			throw new NotFoundResponse(anfe.getMessage());
		} catch (InsufficientBalanceException | InvalidAmountException exception) {
			throw new BadRequestResponse(exception.getMessage());
		} 
    }
    
    /**
     * Credits money into an account. Account id should be in path parameters, the
     * credit amount is passed in the request body via
     * {@link UpdateBalanceRequest}. Used Http.Verb <code>PATCH</code> as it
     * <b>partially</b> updates the account information.
     * 
     * @param context the context.
     */
    @OpenApi(
            summary = "Credit money into given account ID",
            operationId = "credit",
            path = "/accounts/:accountId/credit",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "accountId", type = Integer.class, description = "The account ID")},
            tags = {"Account"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = UpdateBalanceRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Account.class)}),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public void credit(Context context) {
    	try {
    		int accountId = validateIntegerPathParam(context, "accountId");
    		UpdateBalanceRequest updateRequest = context.bodyAsClass(UpdateBalanceRequest.class);
    		// validate requested update amount.
    		validateAmount(updateRequest.getAmount());
    		
    		context.json(repository.credit(accountId, updateRequest.getAmount()));
    		context.status(200);
		} catch (AccountNotFoundException anfe) {
			throw new NotFoundResponse(anfe.getMessage());
		} catch (InvalidAmountException iae) {
			throw new BadRequestResponse(iae.getMessage());
		}
    }
    
    /**
     * Deletes an account with account id, which is extracted from the path parameters of
     * given {@link Context}.
     * @param context the context.
     */
    @OpenApi(
            summary = "Delete account by ID",
            operationId = "deleteById",
            path = "/accounts/:accountId",
            method = HttpMethod.DELETE,
            pathParams = {@OpenApiParam(name = "accountId", type = Integer.class, description = "The account ID")},
            tags = {"Account"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Account.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
	public void deleteByID(Context context) {
		try {
			context.json(repository.delete(validateIntegerPathParam(context, "accountId")));
			context.status(200);
		} catch (AccountNotFoundException anfe) {
			throw new NotFoundResponse(anfe.getMessage());
		}
	}
}
