package com.revolut.tugolut.exception;

import java.text.MessageFormat;

/**
 * Exception indicates an internal service exception happens because of another cause.
 */
public class InternalServiceException extends RuntimeException {

	private static final long serialVersionUID = -8653423089231294000L;
	
	public InternalServiceException(Throwable cause) {
		super(MessageFormat.format("Internal service error happened. Cause: {0}", cause.toString()), cause);
	}

}
