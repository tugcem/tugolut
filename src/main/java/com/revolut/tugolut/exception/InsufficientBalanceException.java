package com.revolut.tugolut.exception;

import java.text.MessageFormat;

import com.revolut.tugolut.model.Account;

/**
 * Exception indicates that the account does not have enough balance to operate.
 */
public class InsufficientBalanceException extends Exception {

	private static final long serialVersionUID = -1185225009757619582L;
	
	public InsufficientBalanceException(Account account) {
		super(MessageFormat.format("Account: {0} does not have enough balance.", account));
	}

}
