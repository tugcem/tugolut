package com.revolut.tugolut.exception;

import java.text.MessageFormat;

/**
 * Exception thrown when input-level validation indicates a transfer from an account to the same account.
 */
public class SelfTransferException extends Exception {

	private static final long serialVersionUID = -1609899938499070412L;
	
	public SelfTransferException(int accountId) {
		super(MessageFormat.format("Self-transfer is not allowed. Requested account id: {0}", accountId));
	}

}
