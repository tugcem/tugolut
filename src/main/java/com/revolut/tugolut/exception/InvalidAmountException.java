package com.revolut.tugolut.exception;

import java.text.MessageFormat;

/**
 * Exception indicates that input-level validation fails for given amount.
 */
public class InvalidAmountException extends Exception {

	private static final long serialVersionUID = 8063471495969499574L;
	
	public InvalidAmountException(double amount) {
		super(MessageFormat.format("Requested amount is invalid: {0}", amount));
	}
}
