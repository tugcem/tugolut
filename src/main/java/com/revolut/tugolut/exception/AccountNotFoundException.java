package com.revolut.tugolut.exception;

import java.text.MessageFormat;

/**
 * Exception indicates that the account with given account id does not exist in
 * the system.
 */
public class AccountNotFoundException extends Exception {

	private static final long serialVersionUID = -1631709305323154059L;

	public AccountNotFoundException(int id) {
		super(MessageFormat.format("Account not found for given account id: {0}", id));

	}
}
