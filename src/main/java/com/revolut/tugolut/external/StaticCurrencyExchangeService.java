package com.revolut.tugolut.external;

import com.google.common.collect.Table;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.revolut.tugolut.model.Currency;

import lombok.extern.slf4j.Slf4j;

/**
 * The implementation of {@link CurrencyExchangeService} where statically
 * convert between supported {@link Currency currencies}.
 */
@Singleton
@Slf4j
public class StaticCurrencyExchangeService implements CurrencyExchangeService {

	@Inject
	@Named("StaticCurrencyExchangeRates")
	Table<Currency, Currency, Double> currencyTable;

	@Override
	public double convert(Currency from, Currency to, double amount) {
		double rate = currencyTable.get(from, to);
		double exchanged = rate * amount;
		
		log.info("{} {} has been exchanged to {} {}", amount, from, exchanged, to);
		return Double.valueOf(exchanged);
	}

}
