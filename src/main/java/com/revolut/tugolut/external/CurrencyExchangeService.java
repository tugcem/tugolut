package com.revolut.tugolut.external;

import com.revolut.tugolut.model.Currency;

/**
 * The interface where implementors converts between supported {@link Currency} units.
 */
public interface CurrencyExchangeService {

	double convert(Currency from, Currency to, double amount);

}
