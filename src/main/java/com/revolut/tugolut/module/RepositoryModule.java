package com.revolut.tugolut.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.revolut.tugolut.repository.AccountRepository;
import com.revolut.tugolut.repository.InMemoryAccountRepository;

/**
 * Module that configures data-layer implementations and dependencies.
 */
public class RepositoryModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides
	@Singleton
	public AccountRepository getAccountRepository() {
		return new InMemoryAccountRepository();
	}

	
}
