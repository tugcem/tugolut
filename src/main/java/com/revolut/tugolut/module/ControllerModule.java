package com.revolut.tugolut.module;

import com.google.inject.AbstractModule;
import com.revolut.tugolut.controller.AccountController;
import com.revolut.tugolut.controller.TransferController;

/**
 * Module to configure API-level controllers.
 */
public class ControllerModule extends AbstractModule{

	@Override
	protected void configure() {
		bind(AccountController.class);
		bind(TransferController.class);
	}
}
