package com.revolut.tugolut.module;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.revolut.tugolut.external.CurrencyExchangeService;
import com.revolut.tugolut.external.StaticCurrencyExchangeService;
import com.revolut.tugolut.model.Currency;

/**
 * Module that configures external dependencies.
 */
public class ExternalDependenciesModule extends AbstractModule {
	
	@Override
	protected void configure() {
		bind(CurrencyExchangeService.class).to(StaticCurrencyExchangeService.class);
	}
	
	/**
	 * Provides a table that contains static values for currency exchange rates. Taken from
	 * <a href="https://www.xe.com/?cn=gb">xe</a> at 2020-03-08T15:08:00Z
	 * 
	 * @return a static currency exchange table.
	 */
	@Provides
	@Singleton
	@Named("StaticCurrencyExchangeRates")
	public Table<Currency, Currency, Double> getCurrencyExchangeRates() {
		Table<Currency, Currency, Double> currencyTable = HashBasedTable.create();

		currencyTable.put(Currency.GBP, Currency.GBP, 1.00000);
		currencyTable.put(Currency.GBP, Currency.EUR, 1.15601);
		currencyTable.put(Currency.GBP, Currency.USD, 1.30498);
		currencyTable.put(Currency.GBP, Currency.TRY, 7.94987);

		currencyTable.put(Currency.EUR, Currency.GBP, 0.86504);
		currencyTable.put(Currency.EUR, Currency.EUR, 1.00000);
		currencyTable.put(Currency.EUR, Currency.USD, 1.12886);
		currencyTable.put(Currency.EUR, Currency.TRY, 6.87698);

		currencyTable.put(Currency.USD, Currency.GBP, 0.76630);
		currencyTable.put(Currency.USD, Currency.EUR, 0.88585);
		currencyTable.put(Currency.USD, Currency.USD, 1.00000);
		currencyTable.put(Currency.USD, Currency.TRY, 6.09196);

		currencyTable.put(Currency.TRY, Currency.GBP, 0.12579);
		currencyTable.put(Currency.TRY, Currency.EUR, 0.14541);
		currencyTable.put(Currency.TRY, Currency.USD, 0.16415);
		currencyTable.put(Currency.TRY, Currency.TRY, 1.00000);

		return currencyTable;
	}
}
