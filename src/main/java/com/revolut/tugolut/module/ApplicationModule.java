package com.revolut.tugolut.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.revolut.tugolut.controller.AccountController;
import com.revolut.tugolut.controller.TransferController;
import com.revolut.tugolut.model.ErrorResponse;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

/**
 * The main Guice module, only this module should be injected to initialize the
 * entire service.
 */
public class ApplicationModule extends AbstractModule {
	
	@Override
	protected void configure() {
		install(new ControllerModule());
		install(new RepositoryModule());
		install(new ExternalDependenciesModule());
	}
	
	@Provides
	@Singleton
	public Javalin getJavalin(AccountController accountController, TransferController transferController) {
		
		Javalin javalin = Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
        })
		.get("/accounts", accountController::getAll)
		.get("/accounts/:accountId", accountController::getByID)
		.delete("/accounts/:accountId", accountController::deleteByID)
		.patch("/accounts/:accountId/withdraw", accountController::withdraw)
		.patch("/accounts/:accountId/credit", accountController::credit)
		.post("/accounts", accountController::create)
		.post("/accounts/:fromAccountId/transfer/:toAccountId", transferController::transfer);
		
		return javalin;
	}
	
	@Provides
	@Singleton
	@Named("Http.Port")
	public int getHttpPort() {
		return 1881;
	}
	
    private OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("Tugolut Accounts API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("com.revolut.tugolut")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/")) // endpoint for swagger-ui, placed on root.
                .defaultDocumentation(doc -> {
                    doc.json("500", ErrorResponse.class);
                    doc.json("503", ErrorResponse.class);
                });
        return new OpenApiPlugin(options);
    }

}
