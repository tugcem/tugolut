package com.revolut.tugolut.repository;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import com.google.common.collect.ImmutableList;
import com.google.inject.Singleton;
import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InternalServiceException;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * A thread-safe, in-memory implementation for {@link AccountRepository}. For the sake of
 * simplification, stores the account information in a {@link ConcurrentHashMap}
 * and restricts access to this map with a {@link ReentrantLock} as concurrent
 * hash map do not entail locking for the entire map during retrieval. Also,
 * uses an {@link AtomicInteger} to keep a counter for new-created account ids.
 */
@Singleton
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class InMemoryAccountRepository implements AccountRepository {

    static AtomicInteger nextID;

    /**
     * Since this class is an in-memory account repository and we need to ensure
     * that we're accessing to the data with an ACID fashion, we need to have a lock
     * to restrict access to the data from multiple threads.
     */
    ReentrantLock lock;

    Map<Integer, Account> accounts = new ConcurrentHashMap<>();

    public InMemoryAccountRepository() {
        nextID = new AtomicInteger(0);
        lock = new ReentrantLock(true);
        accounts = new ConcurrentHashMap<>();
    }

    @Override
    public List<Account> getAll() {
        try {
            lock.lock();
            log.info("Returning all accounts");
            return accounts.values().stream().collect(collectingAndThen(toList(), ImmutableList::copyOf));
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Account getByID(int id) throws AccountNotFoundException {
        try {
            lock.lock();
            if (accounts.containsKey(id)) {
                log.info("Returning account details with id: {}", id);
                return accounts.get(id);
            } else {
                log.warn("Given account id: {} is not found, no account returned", id);
                throw new AccountNotFoundException(id);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Account create(double initialBalance, Currency currency) throws InternalServiceException {
        try {
            lock.lock();

            // get the next internal account id and add the new account.
            int accountID = nextID.incrementAndGet();
            // @formatter:off
			Account newAccount = Account.builder().id(accountID).balance(initialBalance)
					.currency(currency).build();
			// @formatter:on

            accounts.put(accountID, newAccount);

            log.info("New account has been created: {}", newAccount);
            return newAccount;
        } catch (Exception exc) {
            log.error("Exception occurred while creating the account with initial balance: {}, currency: {}",
                    initialBalance, currency, exc);
            throw new InternalServiceException(exc);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Account withdraw(int id, double amount) throws AccountNotFoundException, InsufficientBalanceException {
        try {
            lock.lock();

            if (accounts.containsKey(id)) {
                // get the existing account and balance to withdraw
                Account account = accounts.get(id);

                // handle business logic and update account balance.
                if (account.getBalance() < amount) {
                    log.warn("Account: {} does not have enough balance. Requested withdrawal: {}", account, amount);
                    throw new InsufficientBalanceException(account);
                } else {
                    log.info("{} withdrawn from account: {}", amount, account);
                    account.setBalance(account.getBalance() - amount);
                    return account;
                }
            } else {
                log.warn("Given account id: {} is not found, no update occurred", id);
                throw new AccountNotFoundException(id);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Account credit(int id, double amount) throws AccountNotFoundException {
        try {
            lock.lock();

            if (accounts.containsKey(id)) {
                // get the existing account and balance to credit
                Account account = accounts.get(id);

                log.info("{} is credited to the account: {}", amount, account);
                // handle business logic and update account balance.
                account.setBalance(account.getBalance() + amount);
                return account;
            } else {
                log.warn("Given account id: {} is not found, no update occurred", id);
                throw new AccountNotFoundException(id);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Account delete(int id) throws AccountNotFoundException {
        try {
            lock.lock();
            if (accounts.containsKey(id)) {
                log.info("Deleting account details with id: {}", id);
                return accounts.remove(id);
            } else {
                log.warn("Given account id: {} is not found, no account deleted", id);
                throw new AccountNotFoundException(id);
            }
        } finally {
            lock.unlock();
        }
    }
}