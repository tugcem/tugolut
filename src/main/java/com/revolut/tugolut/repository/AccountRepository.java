package com.revolut.tugolut.repository;

import java.util.List;

import com.revolut.tugolut.exception.AccountNotFoundException;
import com.revolut.tugolut.exception.InsufficientBalanceException;
import com.revolut.tugolut.exception.InternalServiceException;
import com.revolut.tugolut.model.Account;
import com.revolut.tugolut.model.Currency;

/**
 * The interface where implementors should support basic data-related
 * functionalities for {@link Account}.
 */
public interface AccountRepository {

    /**
     * Retrieves all accounts from underlying store.
     * 
     * @return all accounts.
     */
    List<Account> getAll();

    /**
     * Fetches an account by given account id.
     * 
     * @param id the account id.
     * @return found account.
     * @throws AccountNotFoundException if the account is not found in the store.
     */
    Account getByID(int id) throws AccountNotFoundException;

    /**
     * Creates an account with given initial balance for given Currency unit.
     * 
     * @param initialBalance the initial balance of the account.
     * @param currency       the currency of the account.
     * @return created account instance.
     * @throws InternalServiceException if any internal exception happens.
     */
    Account create(double initialBalance, Currency currency) throws InternalServiceException;

    /**
     * Withdraws given amount from account of given account id.
     * 
     * @param id     the account id.
     * @param amount the amount to withdraw.
     * @return the updated account information.
     * @throws AccountNotFoundException     if no account found for given account
     *                                      id.
     * @throws InsufficientBalanceException if requested amount is more than the
     *                                      account balance.
     */
    Account withdraw(int id, double amount) throws AccountNotFoundException, InsufficientBalanceException;

    /**
     * Credits given amount to the account of given account id.
     * 
     * @param id     the account id
     * @param amount the amount to credit.
     * @return the updated account information.
     * @throws AccountNotFoundException if no account found for given account id.
     */
    Account credit(int id, double amount) throws AccountNotFoundException;

    /**
     * Deletes account of given account id.
     * 
     * @param id the account id.
     * @return deleted account information.
     * @throws AccountNotFoundException if no account found for given account id.
     */
    Account delete(int id) throws AccountNotFoundException;
}
