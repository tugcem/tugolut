package com.revolut.tugolut.main;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.revolut.tugolut.module.ApplicationModule;

import io.javalin.Javalin;

/**
 * The main application for 'revolutionary', next-gen 'Tugolut' Accounts API.
 */
public class Application {

	public static void main(String[] args) {
		// create the injector with application module only.
		Injector injector = Guice.createInjector(new ApplicationModule());

		// fetch the configured port and start javalin.
		Integer port = injector.getInstance(Key.get(Integer.class, Names.named("Http.Port")));
		
		injector.getInstance(Javalin.class).start(port);
	}

}
