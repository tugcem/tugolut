package com.revolut.tugolut.model;

import java.util.Map;

import lombok.Builder;
import lombok.Value;

/**
 * Immutable error response object to wrap exceptions handled / thrown in Tugolut.
 */
@Value
@Builder
public class ErrorResponse {
	
    String title;
	
    int status;
	
    String type;
	
    Map<String, String> details;
}