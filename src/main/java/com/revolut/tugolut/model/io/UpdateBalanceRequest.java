package com.revolut.tugolut.model.io;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The request input object to change the balance of the account. Can be used to
 * credit to, withdraw from or transfer between the account(s).
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateBalanceRequest {
	
	double amount;
	
}
