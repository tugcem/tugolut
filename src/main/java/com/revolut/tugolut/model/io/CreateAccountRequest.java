package com.revolut.tugolut.model.io;

import com.revolut.tugolut.model.Currency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The request input object for creating new accounts.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountRequest {

    Double balance;

    Currency currency;
}
