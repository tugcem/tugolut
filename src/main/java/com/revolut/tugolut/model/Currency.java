package com.revolut.tugolut.model;

/**
 * Available currencies for Tugolut. Ideally, these should be maintained,
 * defined and converted by an external service.
 */
public enum Currency {

	GBP, EUR, USD, TRY;
	
}
