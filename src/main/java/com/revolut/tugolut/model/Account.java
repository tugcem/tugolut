package com.revolut.tugolut.model;

import lombok.Builder;
import lombok.Data;

/**
 * Represents a bank account.
 */
@Data
@Builder
public class Account {
	
	int id;
	
	@Builder.Default
	Double balance = 0.0d;
	
	Currency currency;
}
